﻿using System;
using System.Collections.Generic;
using System.Web.Http;

namespace E2U.Mobile.API.Controllers
{
    public class TelemetryController : ApiController
    {
        // GET api/values
        // Returns last N events (for a specified period range)
        [Authorize(Roles = "TelemetryReader")]
        [HttpGet]
        public IEnumerable<string> Get(
            Guid deviceId, int numberOfEvents, 
            DateTime? from = null, DateTime? to = null)
        {
            return new string[] {
                deviceId.ToString(), numberOfEvents.ToString(),
                from?.ToString() ?? "from unspesified", to?.ToString() ?? "to unspesified"
            };
        }
    }
}