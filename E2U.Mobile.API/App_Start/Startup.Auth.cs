﻿using E2U.Mobile.API.Helpers;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Owin.Security.ActiveDirectory;
using Owin;

namespace E2U.Mobile.API
{
    public partial class Startup
    {
        public void ConfigureAuth(IAppBuilder app)
        {
            app.UseWindowsAzureActiveDirectoryBearerAuthentication(
                new WindowsAzureActiveDirectoryBearerAuthenticationOptions
                {
                    Tenant = AppSettings.AadIdaTenant,
                    TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidAudience = AppSettings.AadIdaApiResourceId,
                        RoleClaimType = AppSettings.AadIdaRoleClaimType
                    }
                });
            /*
             * Current API is registered as E2U.Mobile.API with one application representing a user E2U.Mobile.API.User001
             * Following are the available roles for the API:
                "appRoles": [
                    {
                      "allowedMemberTypes": [
                        "Application"
                      ],
                      "displayName": "TelemetryReader",
                      "id": "001b1dc0-4801-46f3-bc0d-35f059da1436",
                      "isEnabled": true,
                      "description": "TelemetryReader",
                      "value": "TelemetryReader"
                    },
                    {
                      "allowedMemberTypes": [
                        "Application"
                      ],
                      "displayName": "ConfigurationReader",
                      "id": "201b1dc0-4801-46f3-bc0d-35f059da1436",
                      "isEnabled": true,
                      "description": "ConfigurationReader",
                      "value": "ConfigurationReader"
                    },
                    {
                      "allowedMemberTypes": [
                        "Application"
                      ],
                      "displayName": "ConfigurationWriter",
                      "id": "202b1dc0-4801-46f3-bc0d-35f059da1436",
                      "isEnabled": true,
                      "description": "ConfigurationWriter",
                      "value": "ConfigurationWriter"
                    }
                ],
             */
        }
    }
}