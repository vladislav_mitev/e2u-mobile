﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(E2U.Mobile.API.Startup))]
namespace E2U.Mobile.API
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}