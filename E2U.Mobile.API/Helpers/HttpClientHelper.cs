﻿using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using static E2U.Mobile.API.Enums;

namespace E2U.Mobile.API.Helpers
{
    public static class HttpClientHelper
    {
        private static readonly HttpClient httpClient;

        static HttpClientHelper()
        {
            httpClient = new HttpClient();
        }

        public static async Task<string> SendToApi(string url, string verb, object obj = null, TargetAPI api = TargetAPI.Any)
        {
            HttpResponseMessage response;

            string token = await GetOath2Token(api);
            if (token != null)
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }

            switch (verb.ToUpper())
            {
                case "GET":
                    response = await httpClient.GetAsync(url);
                    break;
                case "POST":
                    response = await httpClient.PostAsJsonAsync(url, obj);
                    break;
                case "PUT":
                    response = await httpClient.PutAsJsonAsync(url, obj);
                    break;
                case "DELETE":
                    response = await httpClient.DeleteAsync(url);
                    break;
                default:
                    response = await httpClient.GetAsync(url);
                    break;
            }

            var result = await response.Content.ReadAsStringAsync();
            return result;
        }

        //Request token from Azure AD
        private static async Task<string> GetOath2Token(TargetAPI api)
        {
            string api_resourceId = null;
            switch (api)
            {
                case TargetAPI.IoTManagementLight:
                    //CustomerService API is registered as "eSmart.Backend.Service.IoTManagement"
                    api_resourceId = AppSettings.AadIdaIotMgmtResourceId;
                    break;
                case TargetAPI.IoTDirectory:
                    //IoTDirectory API is registered as "E2U.IoTDirectory"
                    api_resourceId = AppSettings.AadIdaIotDirResourceId;
                    break;
                default:
                case TargetAPI.Any:
                    return null;
            }
            
            ClientCredential clientCredentials = new ClientCredential(
                AppSettings.AadIdaApiClientId, AppSettings.AadIdaApiClientSecret);

            AuthenticationContext context = new AuthenticationContext(AppSettings.AadIdaTokenEndpoint);
            AuthenticationResult result = await context.AcquireTokenAsync(api_resourceId, clientCredentials);
            string accessToken = result.AccessToken;
            return accessToken;
        }
    }
}