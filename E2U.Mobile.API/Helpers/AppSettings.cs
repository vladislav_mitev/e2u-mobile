﻿using System;
using System.Configuration;

namespace E2U.Mobile.API.Helpers
{
    public static class AppSettings
    {
        public static string AadIdaTenant => ConfigurationManager.AppSettings["ida:Tenant"] as String;
        public static string AadIdaApiResourceId => ConfigurationManager.AppSettings["ida:ApiResourceId"] as String;
        public static string AadIdaApiClientId => ConfigurationManager.AppSettings["ida:ApiClientId"] as String;
        public static string AadIdaApiClientSecret => ConfigurationManager.AppSettings["ida:ApiClientSecret"] as String;
        public static string AadIdaRoleClaimType => ConfigurationManager.AppSettings["ida:RoleClaimType"] as String;
        public static string AadIdaTokenEndpoint => ConfigurationManager.AppSettings["ida:TokenEndpoint"] as String;
        public static string AadIdaIotMgmtResourceId => ConfigurationManager.AppSettings["ida:IotMgmtResourceId"] as String;
        public static string AadIdaIotDirResourceId => ConfigurationManager.AppSettings["ida:IotDirResourceId"] as String;
    }
}